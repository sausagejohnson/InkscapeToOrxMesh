# Inkscape to Orx Converter Tool #

This tool allows the user to take SVG files from Inkscape, 
and converts the paths therein into mesh lists used by 
Orx Object Body Parts.

### How do I get set up? ###

This is the source version of the Inkscape to Orx converter. You need Visual Studio to compile. 
If you just want to use the software, head on over to the Tutorial
where you can learn to use the tool, and where you can also download
the binary version.

You can also download it directly here.

### Contribution guidelines ###

You are welcome to contribute and make improvements. There are many
things not checked in the SVG that could be improved. If you find something
missing that this tool should support, please add it! 

### Who do I talk to? ###

* Contact Wayne at the <a href="https://orx-project.org/discord">Orx Discord Channel</a>
* Get me on email, check the source for my address, or select 'help'
in the application for my address.
