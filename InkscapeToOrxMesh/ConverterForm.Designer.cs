﻿namespace InkscapeToOrxMesh
{
	partial class ConverterForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConverterForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.previewButton = new System.Windows.Forms.Button();
            this.convertButton = new System.Windows.Forms.Button();
            this.clipboardRadioButton = new System.Windows.Forms.RadioButton();
            this.INIRadioButton = new System.Windows.Forms.RadioButton();
            this.orxBrowseButton = new System.Windows.Forms.Button();
            this.iniTextBox = new System.Windows.Forms.TextBox();
            this.svgBrowseButton = new System.Windows.Forms.Button();
            this.svgTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.baseObjectNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.inheritTextTextBox = new System.Windows.Forms.TextBox();
            this.previewBox = new System.Windows.Forms.TextBox();
            this.previewGroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.offSetXBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.offSetYBox = new System.Windows.Forms.TextBox();
            this.helpTip = new System.Windows.Forms.ToolTip(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.previewGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(637, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem2
            // 
            this.helpToolStripMenuItem2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.helpToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1,
            this.helpToolStripMenuItem3});
            this.helpToolStripMenuItem2.Name = "helpToolStripMenuItem2";
            this.helpToolStripMenuItem2.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem2.Text = "Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem3
            // 
            this.helpToolStripMenuItem3.Name = "helpToolStripMenuItem3";
            this.helpToolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem3.Text = "Help";
            this.helpToolStripMenuItem3.Click += new System.EventHandler(this.helpToolStripMenuItem3_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            // 
            // previewButton
            // 
            this.previewButton.Enabled = false;
            this.previewButton.Location = new System.Drawing.Point(464, 402);
            this.previewButton.Name = "previewButton";
            this.previewButton.Size = new System.Drawing.Size(75, 23);
            this.previewButton.TabIndex = 22;
            this.previewButton.Text = "Preview";
            this.previewButton.UseVisualStyleBackColor = true;
            this.previewButton.Visible = false;
            this.previewButton.Click += new System.EventHandler(this.previewButton_Click);
            // 
            // convertButton
            // 
            this.convertButton.Enabled = false;
            this.convertButton.Location = new System.Drawing.Point(545, 402);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 23);
            this.convertButton.TabIndex = 9;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // clipboardRadioButton
            // 
            this.clipboardRadioButton.AutoSize = true;
            this.clipboardRadioButton.Checked = true;
            this.clipboardRadioButton.Location = new System.Drawing.Point(104, 371);
            this.clipboardRadioButton.Name = "clipboardRadioButton";
            this.clipboardRadioButton.Size = new System.Drawing.Size(109, 17);
            this.clipboardRadioButton.TabIndex = 8;
            this.clipboardRadioButton.TabStop = true;
            this.clipboardRadioButton.Text = "Send to Clipboard";
            this.clipboardRadioButton.UseVisualStyleBackColor = true;
            // 
            // INIRadioButton
            // 
            this.INIRadioButton.AutoSize = true;
            this.INIRadioButton.Location = new System.Drawing.Point(108, 310);
            this.INIRadioButton.Name = "INIRadioButton";
            this.INIRadioButton.Size = new System.Drawing.Size(164, 17);
            this.INIRadioButton.TabIndex = 5;
            this.INIRadioButton.TabStop = true;
            this.INIRadioButton.Text = "Output to ORX Config INI File";
            this.INIRadioButton.UseVisualStyleBackColor = true;
            // 
            // orxBrowseButton
            // 
            this.orxBrowseButton.Location = new System.Drawing.Point(596, 332);
            this.orxBrowseButton.Name = "orxBrowseButton";
            this.orxBrowseButton.Size = new System.Drawing.Size(24, 20);
            this.orxBrowseButton.TabIndex = 7;
            this.orxBrowseButton.Text = "...";
            this.orxBrowseButton.UseVisualStyleBackColor = true;
            this.orxBrowseButton.Click += new System.EventHandler(this.orxBrowseButton_Click);
            // 
            // iniTextBox
            // 
            this.iniTextBox.Enabled = false;
            this.iniTextBox.Location = new System.Drawing.Point(108, 333);
            this.iniTextBox.Name = "iniTextBox";
            this.iniTextBox.Size = new System.Drawing.Size(482, 20);
            this.iniTextBox.TabIndex = 6;
            // 
            // svgBrowseButton
            // 
            this.svgBrowseButton.Location = new System.Drawing.Point(596, 52);
            this.svgBrowseButton.Name = "svgBrowseButton";
            this.svgBrowseButton.Size = new System.Drawing.Size(24, 20);
            this.svgBrowseButton.TabIndex = 2;
            this.svgBrowseButton.Text = "...";
            this.svgBrowseButton.UseVisualStyleBackColor = true;
            this.svgBrowseButton.Click += new System.EventHandler(this.svgBrowseButton_Click);
            // 
            // svgTextBox
            // 
            this.svgTextBox.Enabled = false;
            this.svgTextBox.Location = new System.Drawing.Point(108, 52);
            this.svgTextBox.Name = "svgTextBox";
            this.svgTextBox.Size = new System.Drawing.Size(482, 20);
            this.svgTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(105, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Select Inkscape SVG File";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::InkscapeToOrxMesh.Properties.Resources.inkscape1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::InkscapeToOrxMesh.Properties.Resources.orx;
            this.pictureBox2.Location = new System.Drawing.Point(7, 310);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(94, 50);
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // baseObjectNameTextBox
            // 
            this.baseObjectNameTextBox.Location = new System.Drawing.Point(108, 101);
            this.baseObjectNameTextBox.Name = "baseObjectNameTextBox";
            this.baseObjectNameTextBox.Size = new System.Drawing.Size(236, 20);
            this.baseObjectNameTextBox.TabIndex = 3;
            this.baseObjectNameTextBox.Text = "Object";
            this.baseObjectNameTextBox.Click += new System.EventHandler(this.baseObjectNameTextBox_Click);
            this.baseObjectNameTextBox.Enter += new System.EventHandler(this.baseObjectNameTextBox_Enter);
            this.baseObjectNameTextBox.Leave += new System.EventHandler(this.baseObjectNameTextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Base Object Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(381, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Inherit Text";
            // 
            // inheritTextTextBox
            // 
            this.inheritTextTextBox.Location = new System.Drawing.Point(384, 101);
            this.inheritTextTextBox.Name = "inheritTextTextBox";
            this.inheritTextTextBox.Size = new System.Drawing.Size(214, 20);
            this.inheritTextTextBox.TabIndex = 4;
            this.inheritTextTextBox.Enter += new System.EventHandler(this.inheritTextTextBox_Enter);
            this.inheritTextTextBox.Leave += new System.EventHandler(this.inheritTextTextBox_Leave);
            // 
            // previewBox
            // 
            this.previewBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.previewBox.Location = new System.Drawing.Point(6, 14);
            this.previewBox.Multiline = true;
            this.previewBox.Name = "previewBox";
            this.previewBox.ReadOnly = true;
            this.previewBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.previewBox.Size = new System.Drawing.Size(500, 118);
            this.previewBox.TabIndex = 0;
            // 
            // previewGroupBox
            // 
            this.previewGroupBox.Controls.Add(this.previewBox);
            this.previewGroupBox.Location = new System.Drawing.Point(108, 127);
            this.previewGroupBox.Name = "previewGroupBox";
            this.previewGroupBox.Size = new System.Drawing.Size(512, 138);
            this.previewGroupBox.TabIndex = 31;
            this.previewGroupBox.TabStop = false;
            this.previewGroupBox.Text = "Preview";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(105, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Coordinate Offset X:";
            // 
            // offSetXBox
            // 
            this.offSetXBox.Location = new System.Drawing.Point(209, 265);
            this.offSetXBox.Name = "offSetXBox";
            this.offSetXBox.Size = new System.Drawing.Size(52, 20);
            this.offSetXBox.TabIndex = 33;
            this.offSetXBox.Text = "0";
            this.offSetXBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.offSetXBox_MouseClick);
            this.offSetXBox.Enter += new System.EventHandler(this.offSetXBox_Enter);
            this.offSetXBox.Leave += new System.EventHandler(this.offSetXBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 268);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Y:";
            // 
            // offSetYBox
            // 
            this.offSetYBox.Location = new System.Drawing.Point(290, 265);
            this.offSetYBox.Name = "offSetYBox";
            this.offSetYBox.Size = new System.Drawing.Size(52, 20);
            this.offSetYBox.TabIndex = 35;
            this.offSetYBox.Text = "0";
            this.offSetYBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.offSetYBox_MouseClick);
            this.offSetYBox.Enter += new System.EventHandler(this.offSetYBox_Enter);
            this.offSetYBox.Leave += new System.EventHandler(this.offSetYBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(350, 268);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 15);
            this.label6.TabIndex = 36;
            this.label6.Text = "?";
            this.helpTip.SetToolTip(this.label6, "You can offset all the coordinate values if your \r\nOrx object has a pivot other t" +
        "han the Top Left.");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(604, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 15);
            this.label7.TabIndex = 37;
            this.label7.Text = "?";
            this.helpTip.SetToolTip(this.label7, "This adds an inheritance section name \r\ninto each section. For example: BaseSecti" +
        "on \r\nwould give a result like:\r\n\r\nObjectPart@BaseSection");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(350, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 15);
            this.label8.TabIndex = 38;
            this.label8.Text = "?";
            this.helpTip.SetToolTip(this.label8, "The basic name for each section. For example,\r\nif Object is chosen, the sections " +
        "created will be:\r\n\r\nObject1\r\nObject2\r\nObject3\r\n... etc.");
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 437);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.offSetYBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.offSetXBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.previewGroupBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inheritTextTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.baseObjectNameTextBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.previewButton);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.clipboardRadioButton);
            this.Controls.Add(this.INIRadioButton);
            this.Controls.Add(this.orxBrowseButton);
            this.Controls.Add(this.iniTextBox);
            this.Controls.Add(this.svgBrowseButton);
            this.Controls.Add(this.svgTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ConverterForm";
            this.Text = "Inkscape SVG Path to ORX Mesh Config v";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.previewGroupBox.ResumeLayout(false);
            this.previewGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.Button previewButton;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.RadioButton clipboardRadioButton;
        private System.Windows.Forms.RadioButton INIRadioButton;
        private System.Windows.Forms.Button orxBrowseButton;
        private System.Windows.Forms.TextBox iniTextBox;
        private System.Windows.Forms.Button svgBrowseButton;
        private System.Windows.Forms.TextBox svgTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem3;
        private System.Windows.Forms.TextBox baseObjectNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox inheritTextTextBox;
        private System.Windows.Forms.TextBox previewBox;
        private System.Windows.Forms.GroupBox previewGroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox offSetXBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox offSetYBox;
        private System.Windows.Forms.ToolTip helpTip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

