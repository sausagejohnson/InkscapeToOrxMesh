using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace InkscapeToOrxMesh
{
    public partial class ConverterForm : Form
    {
        const string version = "0.3";
        const string decimalFormat = "#0.##";

        public ConverterForm()
        {
            InitializeComponent();

            this.Text += version;

            

        }

        private void svgBrowseButton_Click(object sender, EventArgs e)
        {
            previewBox.Text = "";

            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string svgFilePath = dialog.FileName;

                svgTextBox.Text = svgFilePath;
                svgTextBox.Enabled = true;

                if (iniTextBox.Text.Length == 0)
                {
                    if (svgFilePath.EndsWith(".svg"))
                    {
                        iniTextBox.Text = svgFilePath.Replace(".svg", ".ini");
                        iniTextBox.Enabled = true;
                    }
                }

                previewButton.Enabled = true;
                convertButton.Enabled = true;

                previewBox.Text = GeneratePreview();

            }
        }

        private void orxBrowseButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string iniFilePath = dialog.FileName;

                iniTextBox.Text = iniFilePath;
                iniTextBox.Enabled = true;

            }
        }

        private string GeneratePreview()
        {
            string output = ConvertSVGtoOrx();
            if (output.Length == 0)
            {
                return "Did not convert.";
            }

            return output;
        }

        private void previewButton_Click(object sender, EventArgs e)
        {
            string output = GeneratePreview();
            MessageBox.Show(output);
        }

        private Coordinates GetCoordsWithOffset(Coordinates current)
        {
            decimal offsetX = 0;
            decimal offsetY = 0;
            if (!decimal.TryParse(offSetXBox.Text, out offsetX))
            {
                return current;
            }

            if (!decimal.TryParse(offSetYBox.Text, out offsetY))
            {
                return current;
            }

            current.X += offsetX;
            current.Y += offsetY;

            return current;
        }

        private string ConvertSVGtoOrx()
        {
            if (svgTextBox.Text.Length == 0)
                return "";

            FileStream file = File.Open(svgTextBox.Text, FileMode.Open);

            StringBuilder sb = new StringBuilder();
            StringBuilder partsListSb = new StringBuilder();

            string baseName = baseObjectNameTextBox.Text;
            string inheritText = inheritTextTextBox.Text;
            if (inheritText.StartsWith("@")){
                inheritText = inheritText.Substring(1);
            }

            partsListSb.AppendFormat("[{0}Body]", baseName);
            partsListSb.AppendLine();
            partsListSb.Append("PartList = ");

            XmlTextReader reader = new XmlTextReader(file);

            int cIndex = 0;

            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    if (reader.Name == "path")
                    {
                        string data = reader["d"].ToString();
                        string[] segments = data.Split(' ');

                        PositionType positionType = PositionType.ABSOLUTE;
                        Coordinates previousCoords = new Coordinates();
                        Coordinates currentCoords = new Coordinates();

                        if (cIndex > 0)
                            partsListSb.Append(" #");
                        partsListSb.AppendFormat("{0}BodyPart{1}", baseName, ++cIndex);

                        sb.AppendFormat("[{1}BodyPart{0}{3}{2}]\r\n", cIndex, baseName, inheritText, (inheritText.Length > 0) ? "@" : "");
                        sb.Append("VertexList = ");

                        int segmentIndex = 0;
                        for (int x = 0; x < segments.Length; x++)
                        {
                            if (segments[x] == "m" || segments[x] == "l")
                            {
                                positionType = PositionType.RELATIVE; //doesn't apply to first coord.
                            }
                            else if (segments[x] == "M" || segments[x] == "L")
                            {
                                positionType = PositionType.ABSOLUTE;
                            }
                            else
                            {
                                if (segments[x].Length > 0 && segments[x].Contains(",")) //not a command, possibly a coord
                                {
                                    if (cIndex == 21)
                                    {
                                        int breakme = 1;
                                    }

                                    string[] coordssplit = segments[x].Split(',');

                                    if (!coordssplit[0].Contains("e")) //skip out these coords if they contain "e"
                                    {
                                        decimal.TryParse(coordssplit[0], out currentCoords.X);
                                        decimal.TryParse(coordssplit[1], out currentCoords.Y);

                                        if (segmentIndex > 0) //for coords beyond the first
                                            sb.Append("#");

                                        if (x == 1) //only for the first coord
                                        {
                                            
                                            Coordinates processedCoords = GetCoordsWithOffset(currentCoords);

                                            previousCoords.X = processedCoords.X;
                                            previousCoords.Y = processedCoords.Y;

                                            sb.AppendFormat("({0}, {1}, 0.0) ", processedCoords.X.ToString(decimalFormat), processedCoords.Y.ToString(decimalFormat));
                                            segmentIndex++;
                                        }
                                        else //for all others.
                                        {
                                            if (positionType == PositionType.RELATIVE)
                                            {
                                                currentCoords.X = previousCoords.X + currentCoords.X;
                                                currentCoords.Y = previousCoords.Y + currentCoords.Y;

                                                sb.AppendFormat("({0}, {1}, 0.0) ", currentCoords.X.ToString(decimalFormat), currentCoords.Y.ToString(decimalFormat));

                                                segmentIndex++;
                                                previousCoords.X = currentCoords.X;
                                                previousCoords.Y = currentCoords.Y;
                                            }
                                            else
                                            {


                                                Coordinates processedCoords = GetCoordsWithOffset(currentCoords);
                                                sb.AppendFormat("({0}, {1}, 0.0) ", processedCoords.X.ToString(decimalFormat), processedCoords.Y.ToString(decimalFormat));
                                                previousCoords.X = processedCoords.X;
                                                previousCoords.Y = processedCoords.Y;
                                                segmentIndex++;
                                            }
                                        }
                                    }

                                    



                                }
                            }
                        }

                        sb.AppendLine("");
                        sb.AppendLine("");

                    }
                }

            }

            file.Close();

            partsListSb.AppendLine("");
            partsListSb.AppendLine("");

            return partsListSb.ToString() + sb.ToString();

        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            string output = ConvertSVGtoOrx();
            if (clipboardRadioButton.Checked)
            {
                Clipboard.Clear();
                Clipboard.SetText(output);
                MessageBox.Show("Orx data copied to the clipboard ready to paste into an Orx ini file.");
            }
            if (INIRadioButton.Checked)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.FileName = iniTextBox.Text;
                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    File.WriteAllText(iniTextBox.Text, output);
                    MessageBox.Show("File saved to " + iniTextBox.Text);
                }

            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Inkscape SVG to Orx Config v" + version + " \n" +
                            "This tool converts Inkscape paths into\n" + 
                            "meshes for use on Object Body Parts\n" +
                            "In Orx.\n\n" +
                            "2017 Wayne Johnson\nsausage@waynejohnson.net");
        }

        private void helpToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            string message = @"
This converter currently expects the following:
1) The SVG path segments must be done in a clockwise order
2) The SVG paths must not have any concave segments
3) The SVG paths can only contain 8 segments
4) The SVG paths must not be grouped

Actually the above is what is expected by Orx / Box2D. The converter won't check this for you.

Use the Preview button to see the output without committing to saving a file or populating your clipboard.

Use the convert button to either save an INI file or to copy to the clipboard.
    
Send bugs to: sausage@zeta.org.au
            ";

            MessageBox.Show(message);
        }

        private void baseObjectNameTextBox_Click(object sender, EventArgs e)
        {
            if (baseObjectNameTextBox.Text == "Object")
                baseObjectNameTextBox.Text = "";

        }

        private void baseObjectNameTextBox_Leave(object sender, EventArgs e)
        {
            if (baseObjectNameTextBox.Text == "")
                baseObjectNameTextBox.Text = "Object";

            previewBox.Text = GeneratePreview();
        }

        private void baseObjectNameTextBox_Enter(object sender, EventArgs e)
        {
        }

        private void inheritTextTextBox_Enter(object sender, EventArgs e)
        {
        }

        private void inheritTextTextBox_Leave(object sender, EventArgs e)
        {
            previewBox.Text = GeneratePreview();
        }

        private void offSetXBox_Leave(object sender, EventArgs e)
        {
            previewBox.Text = GeneratePreview();
        }

        private void offSetYBox_Leave(object sender, EventArgs e)
        {
            previewBox.Text = GeneratePreview();
        }

        private void offSetXBox_Enter(object sender, EventArgs e)
        {
            offSetXBox.SelectAll();
        }

        private void offSetYBox_Enter(object sender, EventArgs e)
        {
            offSetYBox.SelectAll();
        }

        private void offSetXBox_MouseClick(object sender, MouseEventArgs e)
        {
            offSetXBox.SelectAll();
        }

        private void offSetYBox_MouseClick(object sender, MouseEventArgs e)
        {
            offSetYBox.SelectAll();
        }
    }

    public enum PositionType
    {
        ABSOLUTE,
        RELATIVE
    }

    public class Coordinates {
        public decimal X;
        public decimal Y;


    }

}
